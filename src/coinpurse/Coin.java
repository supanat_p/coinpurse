/** coinpurse package*/ 
package coinpurse;
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Supanat Pokturng
 */
public class Coin extends AbstractValuable{

    /** 
     * Value of the coin.
     */
    private double value;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value , String currency ) {
        super(currency);
    	this.value = value;
    }

    /**
     * Get the coin's value.
     * @return the value of this coin
     */
    public double getValue(){
    	return this.value;
    }

    /**
     * toString returns a string description of the coins.
     * @return String of details
     */
    public String toString(){
    	return (int)(this.getValue()) +" "+ this.currency +" Coin" ; 
    }

    
}

