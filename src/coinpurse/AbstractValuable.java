package coinpurse;

/**
 * An abstract valuable with a value.
 * @author Supanat Pokturng
 */
public abstract class AbstractValuable implements Valuable {
	
	protected String currency;
	/**
	 * Constructor of an abstract valuable.
	 * Use form super class
	 */
	public AbstractValuable( String currency ) {
		this.currency = currency;
	}
	
	/**
	 * check value and type of valuable.
	 * @param arg is an object that check
	 * with this valuable
	 * @return true if they value are equals
	 */
	public boolean equals(Object arg) {
		if(arg!= null ){
			if(arg.getClass() == this.getClass()){
				Valuable newObj = (Valuable)arg;
				return (newObj.getValue() == this.getValue());
			}
		}
		return false;
	}
	
	/**
	 * This is method to compare two valuable with value.
	 * @param valuable is other valuable that compare to this
	 * @return integer that pass compare
	 */
	public int compareTo(Valuable valuable){
    	if(this.getValue() < valuable.getValue() )
    		return -1;
    	else if(this.getValue() == valuable.getValue())
    		return 0;
    	else
    		return 1;
    }

}