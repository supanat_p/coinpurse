package coinpurse; 

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A Purse contains coins banknotes and coupons.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the purse decides which
 *  money to remove.
 *  
 *  @author Supanat Pokturng
 */
public class Purse {
    /** Collection of money in the purse. */
    
	private List<Valuable> money; 
	
	private WithdrawStrategy strategy;
	
	/** comparator of money in purse.*/
	private final ValueComparator comparator = new ValueComparator();
    /** Capacity is maximum NUMBER of coins the purse can hold.
     *  Capacity is set when the purse is created.
     */	
    private int capacity;
    
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of money you can put in purse.
     */
    public Purse( int capacity ) {
	
    	this.capacity = capacity;
    	if(capacity >0)
    		money = new ArrayList<Valuable>();
    }
    
    /**
     * Set strategy's attribute.
     * @param strategy is a WithdrawStrategy that want to set
     */
    public void setWithdrawStrategy(WithdrawStrategy strategy) {
    	this.strategy = strategy;
    }

    /**
     * Count and return the number of money in the purse.
     * This is the number of money, not their value.
     * @return the number of money in the purse
     */
    public int count() { 
    	return money.size();
    }
    
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public int getBalance() {
    	double balance = 0;
    	for(int i=0 ; i<money.size() ; i++){
    		balance += money.get(i).getValue();
    	}
    	return (int)balance;
    }
    /**
     * Return the capacity of the purse.
     * @return the capacity
     */
    
    public int getCapacity() { 
    	return this.capacity;
    }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
        
        return this.money.size()>=this.capacity;
    }

    /** 
     * Insert a money into the purse.
     * The money is only inserted if the purse has space for it
     * and the money has positive value.
     * @param valuable is a valuable object to insert into purse
     * @return true if valuable inserted, false if can't insert
     */
    public boolean insert( Valuable valuable ) {
        // if the purse is already full then can't insert anything.
        if(!isFull()){
        	if(valuable.getValue()>0){
        		money.add(valuable);
        		return true;
        	}
        }
        return false;
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Money withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of money objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Valuable[] withdraw( double amount ) {
    	Valuable[] withdrawn = null;
    	if( amount <= 0)
    		return null;
    	if( amount <= this.getBalance()) {
    		withdrawn = strategy.withdraw(amount, money);
    		for( int i=0 ; i<withdrawn.length; i++ ) {
    			money.remove(withdrawn[i]);
    		}
    		
    	}
    	return withdrawn;
	}
  
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     * @return detail of the purse
     */
    public String toString() {
        
    	return String.format("%d valuable with value %.1f",this.count() , this.getBalance());
    }

}

