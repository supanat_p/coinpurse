package coinpurse;

public class ThaiMoneyFactory extends MoneyFactory {
	
	private Valuable valuable = null;
	@Override
	Valuable createMoney(double value) {
		int intValue = (int)value;
		switch (intValue) {
		case 1: case 2: case 5: case 10:
			valuable = new Coin(intValue , "Baht");
			break;
		case 20: case 50: case 100: case 500: case 1000:
			valuable = new BankNote(intValue , "Baht");
			break;
		default:
			throw new IllegalArgumentException();
		}
		return valuable;
	}
	
}
