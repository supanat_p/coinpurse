package coinpurse;
import java.util.Comparator;
/**
 * This interface compare two Valuable.
 * @author Supanat	Pokturng
 */
public class ValueComparator implements Comparator<Valuable> {
	/**
	 * compare value of each valuable.
	 * @param a is a Valuable that compared
	 * @param b is a Valuable that compared
	 * @return the comparable of valuable a and b
	 */
	public int compare(Valuable a, Valuable b) {
		if(a.getValue() < b.getValue())
			return -1;
		else if(a.getValue() == b.getValue())
			return 0;
		else
			return 1;
	}
}
