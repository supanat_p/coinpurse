package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;

public interface WithdrawStrategy {
	Valuable[] withdraw(double amount, List<Valuable> valuable );
}
