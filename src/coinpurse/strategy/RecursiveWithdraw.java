package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;

/**
 * RecursiveWithdraw class use to withdraw valuable
 * from the purse.
 * Not use greedy algorithm 
 * @author Supanat Pokturng
 */
public class RecursiveWithdraw implements WithdrawStrategy {
	
	/**
	 * Use to withdraw valuable from the purse.
	 * not use greedy algorithm
	 * @param amount is number of money that you
	 * want to withdraw
	 * @param list is a List of valuable that you
	 * want to withdraw from
	 * @return array of the Valuable that can withdraw
	 */
	public Valuable[] withdraw(double amount, List<Valuable> list) {
		
		/** Declare a temp's List that keep a valuable's list */
		List<Valuable> tempList = new ArrayList<Valuable>();
		
		/** Declare a withdraw's list that keep valuable witch withdrawn */
		List<Valuable> withdrawList = new ArrayList<Valuable>();
		
		/** Initialize temp's list */
 		for(int i=0 ; i<list.size(); i++ ) {
			tempList.add(list.get(i));
		}
 		
 		/** Sort items in a list */
 		Collections.sort(tempList);
 		
 		/** Declare a newList's list */
 		List<Valuable> newList = new ArrayList<Valuable>();
 		
 		/** Initialize a newList that sort by greater to lower value*/
 		for(int i=tempList.size()-1 ; i>=0; i--) {
 			newList.add(tempList.get(i));
 		}
 		
 		/** Call method to recursive and keep result in arrWithdraw */
 		Valuable[] arrWithdraw = withdrawIndex(amount, newList, withdrawList, 0);
 		
 		/**
 		 * If result is null, it will set new array
 		 * that is not use first index to end index.
 		 */
		if( arrWithdraw == null ) {
			List<Valuable> recurList = new ArrayList<Valuable>();
			recurList = newList.subList(1, newList.size());
			return withdraw(amount, recurList);
		}
	
		return arrWithdraw;
	}
	
	/**
	 * Use to select index of Valuable's list.
	 * if it select but amount is lower than the last index 
	 * it will increase amount with value in that index
	 * @param amount is number of valuable that it want to withdraw
	 * @param list is a list to withdraw a valuable
	 * @param withdrawList is a list of valuable that withdrawn
	 * @param index is index that it wants to withdraw
	 * @return array of valuable that withdrawn
	 */
	public Valuable[] withdrawIndex(double amount, List<Valuable> list, List<Valuable> withdrawList , int index) {
		
		/** 
		 * if amount is less than the value in this index,
		 * it will get that value into withdrawList and reduce amount.
		 */
		if( list.get(index).getValue() <= amount ) {
			amount -= list.get(index).getValue();
			withdrawList.add(list.get(index));
			
			/**
			 * if amount is less than last-index's value,
			 * it will increase amount and remove that value in withdrawList
			 */
			if(amount < list.get(list.size()-1).getValue() && amount != 0) {
				amount += list.get(index).getValue();
				withdrawList.remove(withdrawList.size()-1);
			}
		}
		/** 
		 * if it run until index equals size and amount is not zero,
		 * it will return null
		 */
		if(index == list.size() - 1 && amount!= 0)
			return null;
		
		/**
		 * If amount is equals zero, it will declare a new array and
		 * initialize with withdrawList's value and return it.
		 */
		if(amount == 0){
			Valuable[] arrWithdraw = new Valuable[withdrawList.size()];
			withdrawList.toArray(arrWithdraw);
			return arrWithdraw;
		}
		System.out.println(amount);
		
		/** Run this method again with next index. */
		return withdrawIndex(amount, list, withdrawList, ++index);
		
	}
}