package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;

/**
 * GreedyWithdraw class witch implements WithdrawStrategy interface.
 * Use greedy algorithm to withdraw a valuable from a list
 * @author Supanat Pokturng
 */
public class GreedyWithdraw implements WithdrawStrategy{
	
	/**
	 * Calculate to withdraw a valuable from a valuables's list.
	 * @param amount is a number of valuable that it wants to withdraw
	 * @param valuables is a list of valuable that use to withdraw
	 * @return array of valauble that withddrawn
	 */
    public Valuable[] withdraw( double amount, List<Valuable> valuables ) {
        
	   /*
		* One solution is to start from the most valuable coin
		* in the purse and take any coin that maybe used for
		* withdraw.
		* Since you don't know if withdraw is going to succeed, 
		* don't actually withdraw the coins from the purse yet.
		* Instead, create a temporary list.
		* Each time you see a coin that you want to withdraw,
		* add it to the temporary list and deduct the value
		* from amount. (This is called a "Greedy Algorithm".)
		* Or, if you don't like changing the amount parameter,
		* use a local total to keep track of amount withdrawn so far.
		* 
		* If amount is reduced to zero (or tempTotal == amount), 
		* then you are done.
		* Now you can withdraw the coins from the purse.
		* NOTE: Don't use list.removeAll(templist) for this
		* becuase removeAll removes *all* coins from list that
		* are equal (using Coin.equals) to something in templist.
		* Instead, use a loop over templist
		* and remove coins one-by-one.		
		*/
		if(amount<= 0)
			return null;
    	
		// Did we get the full amount?
		if ( amount > 0 ){
			ArrayList<Valuable> temp1 = (ArrayList<Valuable>) valuables;
			Collections.sort(temp1);
			ArrayList<Valuable> temp2 = new ArrayList<Valuable>();
			for(int i= temp1.size()-1 ; i>=0 ; i--){
				if(temp1.get(i).getValue() <= amount){
					amount -= temp1.get(i).getValue();
					temp2.add(temp1.get(i));
				}
			}
			if(amount != 0){
				return null;
			}
			for(int i=0 ; i<temp2.size(); i++){
				for(int j=0 ; j<valuables.size() ; j++){
					if(temp2.get(i).equals(valuables.get(j))){
						valuables.remove(j);
						break;
					}
				}
			}
			Valuable[] arrValuable = new Valuable[temp2.size()];
			temp2.toArray(arrValuable);
			// any money from Purse yet, there is nothing
			// to put back.
			return arrValuable;
		}

		// Success.
		// Since this method returns an array of money,
		// create an array of the correct size and copy
		// the money to withdraw into the array.
        
        return null;
	}
}
