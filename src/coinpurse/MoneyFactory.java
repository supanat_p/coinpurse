package coinpurse;

import java.util.ResourceBundle;

abstract class MoneyFactory {
	private static MoneyFactory moneyFactory = null;
	
	static MoneyFactory getInstance() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		if ( moneyFactory == null ){
			ResourceBundle bundle = ResourceBundle.getBundle("coinpurse.purse");
			String directory = bundle.getString("moneyfactory");
			moneyFactory = (MoneyFactory)(Class.forName(directory).newInstance());
		}
		return moneyFactory;
	}
	
	abstract Valuable createMoney(double value);
	
	Valuable createMoney(String value) {
		double newValue = Double.parseDouble(value);
		return moneyFactory.createMoney(newValue);
	}


	
}
