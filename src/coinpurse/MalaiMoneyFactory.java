package coinpurse;

public class MalaiMoneyFactory extends MoneyFactory{

	private Valuable valuable = null;
	
	Valuable createMoney(double value) {
		if ( value == 0.05 || value == 0.1 || value == 0.2 || value == 0.5 ) {
			valuable = new Coin(value*100 , "Sen");
		}
		else if ( value == 1 || value == 2 || value == 5 || value == 10 || value == 20 || value == 50 || value == 100 ) {
			valuable = new BankNote(value , "Ringgit");
		}
		else {
			throw new IllegalArgumentException();
		}
		return valuable;
	}
}
