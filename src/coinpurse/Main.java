package coinpurse; 

import coinpurse.strategy.RecursiveWithdraw;

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Supanat POkturng
 */
public class Main {

    /**
     * this is the code to test program.
     * @param args not used
     * @throws ClassNotFoundException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     */
    public static void main( String[] args ) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
//    	 Purse purse = new Purse( 25 );
//    	 purse.setWithdrawStrategy( new RecursiveWithdraw());
//    	 ConsoleDialog console = new ConsoleDialog(purse);
//    	 console.run();
    	MoneyFactory factory = MoneyFactory.getInstance();
    	System.out.println( factory.createMoney("10") );
    	System.out.println( factory.createMoney(0.05) );

    }
}
