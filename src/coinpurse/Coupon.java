package coinpurse;
/**
 * A coupon with a value.
 * we can't change the value of coupon
 * @author Supanat Pokturng
 */
public class Coupon extends AbstractValuable{
	
	/** Color of coupon.*/
	private String color;
	/** Type of the coupon.*/
	enum CouponTypes{
		RED(100), BLUE(50), GREEN(20);
		
		public final double value;
		
		CouponTypes(double value){
			this.value = value;
		}
		
	};
	
	/**
	 * Constructor for a coupon.
	 * @param color is color of the coupon
	 */
	public Coupon(String color){
		this.color = color.toUpperCase();
	}
	
	/**
	 * Get the coupon's value.
	 * @return value of the coupon
	 */
	public double getValue(){
		return CouponTypes.valueOf(this.color).value;
	}
	
	/**
	 *toString returns a string description of the coupon.
     *@return String of details
	 */
	public String toString(){
		return this.color.toLowerCase() + " coupon";
	}
}