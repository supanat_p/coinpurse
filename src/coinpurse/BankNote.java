package coinpurse;
/**
 * A BankNote with a monetary value.
 * You can't change the value of a coin.
 * @author Supanat Pokturng
 */
public class BankNote extends AbstractValuable{
	private double value;
	private int serialNumber;
	private static int nextSerialNumber = 1000000;
	/**
	 * Constructor of a new Banknote.
	 * @param value is the value that banknote has
	 */
	public BankNote(double value , String currency ){
		super(currency);
		this.value = value;
		this.serialNumber = getNextSerialNumber();
		nextSerialNumber++;
	}
	/**
	 * use to set a serialnumber of the banknote.
	 * @return next serial that banknote has
	 */
	public static int getNextSerialNumber(){
		return nextSerialNumber;
	}
	/**
	 * use to get a value of banknote.
	 * @return value of banknote
	 */
	public double getValue(){
		return this.value;
	}
	/**
	 * get the description of this Banknote.
	 * @return String of description
	 */
	public String toString(){
		return String.format("%.0f %s BankNote", this.getValue() , this.currency);
	}
}