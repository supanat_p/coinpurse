package coinpurse;
/**
 * This interface return value of each object.
 * that implements this interface
 * @author Supanat Pokturng
 */
public interface Valuable extends Comparable<Valuable> {
	/**
	 * Get the valuable's value.
	 * @return the value of each Valuable
	 */
	public double getValue();
}
